"My Magic Mirror" it's a program for Raspberry Pi written on Java.

How to set up MyMagicMirror

1. Create a directory "MyMagicMirror";
 2. Copy directories "weatherIcons" with icons and "config" with "config.xml" into created "MyMagicMirror" directory;
 3. Compile file MyMagicMirror.jar into "MyMagicMirror" folder.
 4. Paste your weather API key into "config.xml" by path "weather.APIkey" gotten from the openweathermap.org ;
 5. Paste your location API key into "config.xml" by path "location .APIkey" gotten from the api.ipstack.com ;
 6. Run program by the command: "java -jar MyMagicMirror.jar"
 
**P.S.**: If you don't execute steps 4 and 5, you be unable to saw weather information block, 
the same if the internet connection will be interrupted.
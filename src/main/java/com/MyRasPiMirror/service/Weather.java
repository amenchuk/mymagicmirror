package com.MyRasPiMirror.service;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import java.text.DecimalFormat;

public class Weather extends ServiceData {

    private String domainWeatherAPI;
    private String domainLocationAPI;
    private String pathWeatherLocation;
    private String pathLocation;

    private JsonPath jsonPath;
    private double myLatitude = 0;
    private double myLongitude = 0;

    public Weather() {
        domainWeatherAPI = getDomainWeatherAPI();
        domainLocationAPI = getDomainLocationAPI();
        pathWeatherLocation = getPathWeatherLocation();
        pathLocation = getPathLocation();
    }

    private void setGeoLoaction(String publicIP, String locationApiKey){
        RestAssured.baseURI = domainLocationAPI;
        Response response;

           response = RestAssured.
                   given().
                   request().
                   get(String.format(pathLocation, publicIP, locationApiKey));
        jsonPath = response.then().
                contentType(ContentType.JSON).
                extract().
                response().jsonPath();
        myLatitude  = Double.parseDouble(jsonPath.get("latitude").toString());
        myLongitude = Double.parseDouble(jsonPath.get("longitude").toString());
    }

    public boolean getWeatherByLocation(String publicIP, String locationApiKey, String weatherApiKey){
        boolean weatherPresent;

        if(locationApiKey.equals("")){
            System.err.println("Location API key is empty.");
            System.exit(1);
        }
        if(weatherApiKey.equals("")){
            System.err.println("Weather API key is empty.");
            System.exit(1);
        }

            setGeoLoaction(publicIP,locationApiKey);

        if(myLatitude != 0 && myLongitude !=0) {
            RestAssured.baseURI = domainWeatherAPI;
            Response response = RestAssured.
                    given().
                    request().
                    get(String.format(pathWeatherLocation, myLatitude, myLongitude, weatherApiKey));

            if (response.getStatusCode() == 200) {
                weatherPresent = true;
                jsonPath = response.
                        then().
                        contentType(ContentType.JSON).
                        extract().
                        response().jsonPath();
                System.out.println(jsonPath.get("$").toString());
            } else {
                weatherPresent = false;
            }
        }else {
            weatherPresent = false;
            System.out.println("Can't identify geo location.");
        }
        return weatherPresent;
    }

    public String getWeatherMain(){
        return jsonPath.get("weather[0].main").toString();
    }

    public String getCityName(){
        return jsonPath.get("name").toString()+" "+jsonPath.get("sys.country").toString();
    }

    public String getTemperature(){
        double tempInKelvin;
        double tempInCelsius;
        tempInKelvin    = Double.parseDouble(jsonPath.get("main.temp").toString());
        tempInCelsius   = tempInKelvin - 273.15;
        return new DecimalFormat("#0.0").format(tempInCelsius);
    }

    public String getHumidity(){
        return jsonPath.get("main.humidity").toString();
    }

    public String getWindSpeed(){
        return jsonPath.get("wind.speed").toString();
    }

    public int getWeatherCode(){
        int weatherCode = Integer.parseInt(jsonPath.get("weather[0].id").toString());
        if(weatherCode != 800) {
            weatherCode = (int)(weatherCode/100.00);
        }
        return weatherCode;
    }
}

package com.MyRasPiMirror.service;

import io.restassured.path.json.JsonPath;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.net.InetAddress;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ServiceData {

    private String weatherApiKey = "";
    private String geoLocationApiKey = "";

    private String domainLocationAPI    = "http://api.ipstack.com";
    private String pathLocation         = "/%s?access_key=%s";

    private String domainWeatherAPI     = "http://api.openweathermap.org";
    private String pathWeatherLocation  = "/data/2.5/weather?lat=%f&lon=%f&APPID=%s";
    private String pathToIcon           = "weatherIcons/%s%s.png";
    private String pathToIndicators     = "weatherIcons/indicators/%s.png";

    private boolean connectedToInet = false;
    private boolean foundPublicIP = false;
    private String publicIP = "";

    private String fontName = "Verdana";
    private Color fontColor = Color.WHITE;
    private int dateFontSize = 18;
    private int timeFontSize = 54;
    private int weatherFontSize = 18;
    private int indicatorsFontSize = 21;
    public static final String HUMIDITY = "humidity";
    public static final String TEMPERATURE = "temperature";
    public static final String WINDSPEED = "windspeed";


    public Color getFontColor() {
        return fontColor;
    }

    public int getIndicatorsFontSize() {
        return indicatorsFontSize;
    }

    public int getWeatherFontSize() {
        return weatherFontSize;
    }

    public String getFontName() {
        return fontName;
    }

    public int getTimeFontSize() {
        return timeFontSize;
    }

    public int getDateFontSize() {
        return dateFontSize;
    }

    public String getDomainWeatherAPI() {
        return domainWeatherAPI;
    }

    public String getDomainLocationAPI() {
        return domainLocationAPI;
    }

    public String getPathWeatherLocation() {
        return pathWeatherLocation;
    }

    public String getPathLocation() {
        return pathLocation;
    }

    public String getWeatherApiKey() {
        return weatherApiKey;
    }

    public String getGeoLocationApiKey() {
        return geoLocationApiKey;
    }

    public boolean isConnectedToInet() {
        return connectedToInet;
    }

    public boolean isFoundPublicIP() {
        return foundPublicIP;
    }

    public String getPublicIP() {
        return publicIP;
    }

    public ImageIcon getIconImage(int weatherCode, int size){
        String partOfDayLetter = "";
        int currentHour = Integer.parseInt(new SimpleDateFormat("HH").format(new Date(System.currentTimeMillis())));
        if(currentHour>=6 && currentHour <=16){ partOfDayLetter = "d"; }
        else if(currentHour>=17 || currentHour <=5) { partOfDayLetter = "n"; }
        return new ImageIcon(new ImageIcon(String.format(pathToIcon, weatherCode, partOfDayLetter))
                .getImage()
                .getScaledInstance(size, size,  Image.SCALE_SMOOTH));
    }

    public ImageIcon getIndicatorsImage(String indicator, int size){
        if(!new File(String.format(pathToIndicators, indicator)).exists()){
            System.err.println("[ERROR] Image/icon file was not found by the path '"+
                    String.format(pathToIndicators, indicator)+"'.");
            System.exit(0);
        }
        ImageIcon imageIcon = new ImageIcon(String.format(pathToIndicators, indicator));
        Image image = imageIcon.getImage();
        Image newimg = image.getScaledInstance(size, size,  Image.SCALE_SMOOTH);
        return new ImageIcon(newimg);
    }

    public boolean checkInternetConnection(){
        try {
            connectedToInet = InetAddress.getByName("8.8.8.8").isReachable(1000);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return connectedToInet;
    }

    public void checkPublicIP(){
        try {
            publicIP = new BufferedReader(new InputStreamReader(new URL("http://checkip.amazonaws.com").openStream())).readLine().trim();
            if(!publicIP.equals("") && publicIP != null ){
                foundPublicIP = true;
            }
        } catch (IOException e) {
            foundPublicIP = false;
            e.printStackTrace();
        }
    }

    public void readConfigFile() {
        JsonPath configJSON = null;
        File configFile = new File("config/config.json");

        if (!configFile.exists()) {
            System.err.println("[ERROR] Configuration file 'config.json' was not found by the path './MyMagicMirror/config/'.");
            System.exit(1);
        }

        try {
            configJSON = new JsonPath(new FileReader(configFile));
        } catch (FileNotFoundException e) {
            System.err.println("[ERROR] Configuration file 'config.json' was not found by the path './MyMagicMirror/config/'.");
            System.exit(1);
        }
        weatherApiKey = configJSON.get("weather[0].APIkey").toString();
        geoLocationApiKey = configJSON.get("location[0].APIkey").toString();
        System.out.println("Weather API Key:"+weatherApiKey);
        System.out.println("Location API Key:"+geoLocationApiKey);
    }
}

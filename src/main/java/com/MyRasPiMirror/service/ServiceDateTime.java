package com.MyRasPiMirror.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ServiceDateTime {

    public String getCurrentTime(){
            Date d = new Date(System.currentTimeMillis());
            DateFormat f = new SimpleDateFormat("HH:mm:ss");
            return f.format(d);
    }

    public String getLocalDate(){
        Date d = new Date(System.currentTimeMillis());
        DateFormat f = new SimpleDateFormat("dd MMMM yyyy, EEEE");
       return f.format(d);
    }

}

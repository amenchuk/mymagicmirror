package com.MyRasPiMirror.gui;

import com.MyRasPiMirror.service.ServiceData;
import com.MyRasPiMirror.service.ServiceDateTime;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class GUIDateTime extends JPanel {

    private ServiceData data;
    private JLabel lblTime;
    private JLabel lblDate;

    private ServiceDateTime currentDateTime;

    private String fontName;
    private int fontStyle;

    private Timer timeUpdate;
    private Timer dateUpdate;
    private Timer datetimeInit;

    public GUIDateTime(){
        lblDate = new JLabel();
        lblTime = new JLabel();
        data = new ServiceData();
        fontName = data.getFontName();
        fontStyle = Font.BOLD;
        currentDateTime = new ServiceDateTime();
        timeUpdate = new Timer(1000, this::updateTime);
        dateUpdate = new Timer(300000, this::updateDate);
        datetimeInit = new Timer(500, this::initDateTime);

        setBorder(BorderFactory.createEmptyBorder(50,20,5,5)); //set borders
        setLayout(new BorderLayout()); //Layout setting
        setLblTime();
        setLblDate();

        add(lblDate, BorderLayout.NORTH);
        add(lblTime, BorderLayout.CENTER);

        timerStart();

    }

    private void setLblTime(){
        lblTime.setText(currentDateTime.getCurrentTime());
        lblTime.setForeground(Color.WHITE);
        lblTime.setFont(new Font(fontName, fontStyle, data.getTimeFontSize()));
        lblTime.setVerticalAlignment(SwingConstants.TOP);
        lblTime.setHorizontalAlignment(SwingConstants.CENTER);
    }

    private void setLblDate(){
        lblDate.setText(currentDateTime.getLocalDate());
        lblDate.setText(new ServiceDateTime().getLocalDate());
        lblDate.setForeground(data.getFontColor());
        lblDate.setFont(new Font(fontName, fontStyle, data.getDateFontSize()));
        lblDate.setVerticalAlignment(SwingConstants.BOTTOM);
        lblDate.setHorizontalAlignment(SwingConstants.CENTER);
    }

    private void updateTime(ActionEvent e) {
        lblTime.setText(new ServiceDateTime().getCurrentTime());
    }

    private void updateDate(ActionEvent e) {
        lblDate.setText(new ServiceDateTime().getLocalDate());
    }

    private void initDateTime(ActionEvent e) {
        lblTime.setText(new ServiceDateTime().getCurrentTime());
        lblDate.setText(new ServiceDateTime().getLocalDate());
        datetimeInit.stop();
    }

    public void timerStart(){
        datetimeInit.start();
        timeUpdate.start();
        dateUpdate.start();
        System.out.println("Time updating was STARTED!");
    }

    public void timerStop(){
        lblTime.setText(null);
        lblDate.setText(null);
        timeUpdate.stop();
        dateUpdate.stop();
        System.out.println("Time updating was STOPPED!");
    }

}

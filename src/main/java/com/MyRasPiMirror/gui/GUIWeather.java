package com.MyRasPiMirror.gui;

import com.MyRasPiMirror.service.ServiceData;
import com.MyRasPiMirror.service.Weather;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class GUIWeather extends JPanel {
    private JLabel lblWeather;
    private JPanel panelIndicators;
    private JPanel panelIcons;
    private JLabel lblIcon;
    private JLabel lblTemperatureIcon;
    private JLabel lblHumidityIcon;
    private JLabel lblWindSpeedIcon;
    private JLabel lblTemperature;
    private JLabel lblHumidity;
    private JLabel lblWindSpeed;
    private JLabel lblLocation;

    private ServiceData data;
    private String weatherApiKey;
    private String geoLocationApiKey;

    private String fontName;
    private int width;
    private int height;
    private GroupLayout mainLayout;
    private Weather currentWeather;

    private Timer timerInitWeather;
    private Timer timerUpdateWeather;

    public GUIWeather(){


        panelIndicators = new JPanel();
        lblTemperature = new JLabel();
        lblHumidity = new JLabel();
        lblWindSpeed = new JLabel();
        panelIcons = new JPanel();
        lblTemperatureIcon = new JLabel();
        lblHumidityIcon = new JLabel();
        lblWindSpeedIcon = new JLabel();
        lblWeather = new JLabel();
        lblIcon = new JLabel();
        lblLocation = new JLabel();
        data = new ServiceData();
        data.readConfigFile();
        weatherApiKey = data.getWeatherApiKey();
        geoLocationApiKey = data.getGeoLocationApiKey();
        fontName = data.getFontName();
        mainLayout = new GroupLayout(this);
        currentWeather = new Weather();
        timerInitWeather = new Timer(100, this::resizeElementsAction);
        timerUpdateWeather = new Timer(300000, this::updateWeatherAction);

        GridLayout indicatorsLayout = new GridLayout(3, 0);

        panelIndicators.setOpaque(false);
        panelIndicators.setLayout(indicatorsLayout);
        panelIndicators.add(lblTemperature);
        panelIndicators.add(lblHumidity);
        panelIndicators.add(lblWindSpeed);

        panelIcons.setLayout(indicatorsLayout);
        panelIcons.setOpaque(false);
        panelIcons.add(lblTemperatureIcon);
        panelIcons.add(lblHumidityIcon);
        panelIcons.add(lblWindSpeedIcon);

        timerStart();
    }

    private void setSizeWeatherElements(){
        int gap = 1;
        width = this.getSize().width;
        height =this.getSize().height;

        setLayout(mainLayout);

        mainLayout.setHorizontalGroup(
                mainLayout.createSequentialGroup()
                        .addGroup(mainLayout.createParallelGroup().addGap(gap)
                                .addComponent(lblWeather,GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, this.width)
                                .addGroup(mainLayout.createSequentialGroup()
                                        .addComponent(lblIcon,GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, (int)(this.width*0.5)).addGap(gap)
                                        .addComponent(panelIcons,GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, (int)(this.width*0.1)).addGap(gap)
                                        .addComponent(panelIndicators,GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, (int)(this.width*0.4))).addGap(gap)
                                .addComponent(lblLocation,GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, this.width).addGap(gap)));

        mainLayout.setVerticalGroup(
                mainLayout.createSequentialGroup()
                        .addComponent(lblWeather,GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, (int) ((this.height-4)*0.15)).addGap(gap)
                        .addGroup(mainLayout.createParallelGroup()
                                .addComponent(lblIcon,GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, (int) ((this.height-4)*0.75)).addGap(gap)
                                .addComponent(panelIcons,GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, (int) ((this.height-4)*0.75)).addGap(gap)
                                .addComponent(panelIndicators,GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, (int) ((this.height-4)*0.75))).addGap(gap)
                        .addComponent(lblLocation,GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, (int) ((this.height-4)*0.15)));
    }

    private void setUpJLabelText(JLabel jLabel, String text, int vertAlig, int horizAlig, int fontSize){
        jLabel.setText(text);
        jLabel.setForeground(data.getFontColor());
        jLabel.setVerticalAlignment(vertAlig);
        jLabel.setHorizontalAlignment(horizAlig);
        jLabel.setFont(new Font(fontName, Font.BOLD, Math.round(fontSize)));
    }

    private int setIconSize(double coefficient){
        return (int)(Math.min(width,height)*coefficient);
    }

    private void clearLabels(){
        lblWeather.setText("");
        lblIcon.setText("");
        lblTemperature.setText("");
        lblHumidity.setText("");
        lblWindSpeed.setText("");
        lblLocation.setText("");

        lblIcon.setIcon(null);
        lblTemperatureIcon.setIcon(null);
        lblHumidityIcon.setIcon(null);
        lblWindSpeedIcon.setIcon(null);
    }

    private void printCurrentWeather(){
        setUpJLabelText(lblWeather,     currentWeather.getWeatherMain(),SwingConstants.BOTTOM, SwingConstants.CENTER,data.getWeatherFontSize());

        setUpJLabelText(lblIcon,     null,SwingConstants.CENTER, SwingConstants.RIGHT,data.getWeatherFontSize());

        setUpJLabelText(lblTemperature, currentWeather.getTemperature()+"° C",SwingConstants.CENTER, SwingConstants.LEFT, data.getIndicatorsFontSize());
        setUpJLabelText(lblHumidity,    currentWeather.getHumidity()+" %",SwingConstants.CENTER, SwingConstants.LEFT, data.getIndicatorsFontSize());
        setUpJLabelText(lblWindSpeed,   currentWeather.getWindSpeed()+" m/s",SwingConstants.CENTER, SwingConstants.LEFT, data.getIndicatorsFontSize());

        setUpJLabelText(lblLocation,    currentWeather.getCityName(),SwingConstants.TOP,SwingConstants.CENTER, data.getWeatherFontSize());

        setUpJLabelText(lblTemperatureIcon, "",SwingConstants.CENTER, SwingConstants.CENTER, data.getIndicatorsFontSize());
        setUpJLabelText(lblHumidityIcon,    "",SwingConstants.CENTER, SwingConstants.CENTER, data.getIndicatorsFontSize());
        setUpJLabelText(lblWindSpeedIcon,   "",SwingConstants.CENTER, SwingConstants.CENTER, data.getIndicatorsFontSize());

        lblTemperatureIcon.setIcon(data.getIndicatorsImage(data.TEMPERATURE,setIconSize(0.11)));
        lblHumidityIcon.setIcon(data.getIndicatorsImage(data.HUMIDITY,setIconSize(0.11)));
        lblWindSpeedIcon.setIcon(data.getIndicatorsImage(data.WINDSPEED,setIconSize(0.11)));
        lblIcon.setIcon(data.getIconImage(currentWeather.getWeatherCode(),setIconSize(0.7)));
    }

    private void updateWeather(){
            setSizeWeatherElements();
            data.checkInternetConnection();
            if(data.isConnectedToInet()) {
                data.checkPublicIP();
                if(data.isFoundPublicIP()){
                    if(currentWeather.getWeatherByLocation(data.getPublicIP(), geoLocationApiKey, weatherApiKey)){
                        printCurrentWeather();
                    }else System.out.println("No weather...");
                }else System.out.println("Can't get public IP address!");
            }else if(!data.isConnectedToInet()){
                System.out.println("No Internet connection...");
                clearLabels();
                timerInitWeather.stop();
            }
            timerInitWeather.stop();
    }

    private void resizeElementsAction(ActionEvent e) {
        System.out.println("Action Init elements!");
        updateWeather();
    }

    private void updateWeatherAction(ActionEvent e) {
        System.out.println("Action Update Weather!");
        updateWeather();
    }

    public void timerStop(){
        timerInitWeather.stop();
        timerUpdateWeather.stop();
        clearLabels();
        System.out.println("Weather updating was STOPPED!");
    }

    public void timerStart(){
        timerInitWeather.start();
        timerUpdateWeather.start();
        System.out.println("Weather updating was STARTED!");
    }
}

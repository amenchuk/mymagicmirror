package com.MyRasPiMirror.gui;

import javax.swing.*;
import java.awt.*;

public class GUI extends JFrame {

    public GUIDateTime guiDateTime = new GUIDateTime();
    public GUIWeather guiWeather = new GUIWeather();


    public GUI() {

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //setExtendedState(JFrame.MAXIMIZED_BOTH);
        //setUndecorated(true); //display witout header
        setSize(768, 1366);
        getContentPane().setBackground(Color.BLACK);
        setLayout(new GridLayout(4, 2, 5, 5));

        //Initializing panels
        JPanel panel1 = guiDateTime;
        panel1.setOpaque(false); // Make the panel transparent

        JPanel panel2 = new JPanel();
        panel2.setOpaque(false);

        JPanel panel3 = guiWeather;
        panel3.setOpaque(false); // make panel transparent

        JPanel panel4 = new JPanel();
        panel4.setOpaque(false);

        JPanel panel5 = new JPanel();
        panel5.setOpaque(false);

        JPanel panel6 = new JPanel();
        panel6.setOpaque(false);

        JPanel panel7 = new JPanel();
        panel7.setOpaque(false);

        JPanel panel8 = new JPanel();
        panel8.setOpaque(false);

        //Initializing all panels
        add(panel1);
        add(panel2);
        add(panel3);
        add(panel4);
        add(panel5);
        add(panel6);
        add(panel7);
        add(panel8);

        setVisible(true);
    }
}
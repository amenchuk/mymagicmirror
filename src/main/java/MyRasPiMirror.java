import com.MyRasPiMirror.gui.GUI;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.*;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class MyRasPiMirror {
    public static void main(String[] args){

        System.err.println(">>>>>>>>>>>>>>> MyMagicMirror was STARTED <<<<<<<<<<<<<<<<<<<<<");

        GUI gui = null;
        try {
            gui = new GUI();
        }catch (HeadlessException e){
            System.out.println("Display not found\n"+e.getMessage());
            System.exit(1);
        }

        final GpioController gpio = GpioFactory.getInstance();
        GpioPinDigitalInput myButton = gpio.provisionDigitalInputPin(RaspiPin.GPIO_02,"TouchSensor");
        myButton.setShutdownOptions(true);
        myButton.addListener(new GpioPinListenerDigital(){
            @Override
            public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
                String currentTime = new SimpleDateFormat("HH:mm:ss").format(new Date(System.currentTimeMillis()));
                if(event.getState() == PinState.HIGH){
                    System.err.println("["+currentTime+"] State of "+event.getPin()+" - HIGH");
                }
                if(event.getState() == PinState.LOW){
                    System.err.println("["+currentTime+"] State of "+event.getPin()+" - LOW");
                }
            }
        });


        Scanner sc = new Scanner(System.in);
        int number = 0;

        while(true){
            number = sc.nextInt();

            if(number == 1){
                gui.guiDateTime.timerStart();
                gui.guiWeather.timerStart();
            }

            if(number == 0){
                gui.guiDateTime.timerStop();
                gui.guiWeather.timerStop();
            }
        }

    }
}
